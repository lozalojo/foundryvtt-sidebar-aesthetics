export class PortraitConfig extends FormApplication {
  constructor(object, options) {
    super(object || PortraitConfig.defaultSettings, options);
  }

  async getData() {
    let settings = await game.settings.get("sidebar-aesthetics", "portraitConfig");
    settings = mergeObject(PortraitConfig.defaultSettings, settings);
    return {
      data: settings,
      config: CONFIG.SidebarAesthetics,
      usePlayerColor: settings.borderColor === "player",
    };
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      title: game.i18n.localize("SETTINGS.saRollPortraitN"),
      id: "portrait-config",
      template: "modules/sidebar-aesthetics/templates/settings/portrait.html",
      width: 480,
      height: "auto",
    });
  }

  static get defaultSettings() {
    return {
      size: "medium",
      borderColor: "#000000",
      borderSize: 1,
      showOn: {
        rawMessages: true,
        contextMessages: true,
      },
      portraitType: "portrait",
    };
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find('button[name="submit"]').click(this._onSubmit.bind(this));
  }

  /**
   * This method is called upon form submission after form data is validated.
   * @override
   */
  async _updateObject(event, formData) {
    const settings = expandObject(formData);

    // Set player-colored borders
    if (formData["usePlayerColor"] === true) {
      settings.borderColor = "player";
    }
    else if (formData["borderColor"] === "player") {
      settings.borderColor = "#000000";
    }

    // Remove redundant data from settings
    if (settings["usePlayerColor"]) delete settings["usePlayerColor"];

    await game.settings.set("sidebar-aesthetics", "portraitConfig", settings);
  }
}
