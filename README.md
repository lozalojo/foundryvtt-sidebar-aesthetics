# Sidebar Aesthetics

**Type**: Module\
**Description**: Adds optional changes to the sidebar.

## Changes

* Adds an option to show images in the sidebar for Journal entries that have images assigned to them.
* Adds an option to show actors' tokens instead of their portraits in the sidebar.

## Installation

1. Open the main screen of FoundryVTT.
2. Go to the 'Add-on Modules' tab.
3. Click the button 'Install Module'.
4. Enter the following in the Manifest URL textbox: `https://gitlab.com/Furyspark/foundryvtt-sidebar-aesthetics/-/raw/master/module.json`.
5. Click 'Install'.

## Usage

1. Activate the module in your world.
2. Alter the options to your liking.
